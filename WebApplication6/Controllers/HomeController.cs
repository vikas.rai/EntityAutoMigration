﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models;
using System.Data.Entity.Migrations;
using System.Net;
using System.Text;
using HtmlAgilityPack;
using System.IO;
using System.Drawing;
namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {
        context c = new context();
        public ActionResult Index()
        {
            var a = c.User.ToList();
            // var p = c.Chaters.ToList();
            IList<Users1> Users1 = new List<Users1>();

            Users1.Add(new Users1() { id = 1 });
            Users1.Add(new Users1() { id = 2, name = "aa2", username = "u2" });
            Users1.Add(new Users1() { id = 3, name = "aa3", username = "u3" });


            foreach (Users1 std in Users1)
                c.User1.Add(std);
            c.SaveChanges();
            var a1 = c.User1.ToList();

            return View();
        }

        public string ParseHtml()
        {
            string s = "";
            var url = "https://www.aliexpress.com/item/simple-necklace-choker-necklace-cool-necklace-women-C684/32667883386.html?ws_ab_test=searchweb0_0,searchweb201602_3_10152_10065_10151_10068_10084_10083_10119_10080_10082_10081_10110_10136_10137_10111_10060_10138_10112_10113_10062_10114_10141_126_10056_10055_10054_10059_10099_10078_10079_10103_10073_10102_10096_10070_10148_10123_10120_10147_10052_10053_10124_10142_10107_10050_10143_10051,searchweb201603_6,afswitch_1_afChannel,ppcSwitch_2&btsid=9759e785-21d6-4d50-bee1-7b088e6a81e9&algo_expid=a10e3349-2cfa-4fbb-a6b3-3b4cf14c977c-0&algo_pvid=a10e3349-2cfa-4fbb-a6b3-3b4cf14c977c";
            //url="http://www.c-sharpcorner.com";
            using (WebClient client = new WebClient())
            {
                s = client.DownloadString(url);
            }

            HtmlWeb web = new HtmlWeb();
            HtmlDocument document = web.Load(url);
            //HtmlNode node = document.DocumentNode.SelectNodes("//h1[@class='product-name']").First();
            //var ht = node.InnerHtml;
            HtmlNode[] aNodes = document.DocumentNode.SelectNodes(".//a").ToArray();

            //Approach 2  
            // HtmlNode[] aNodes2 = document.DocumentNode.SelectNodes("//h1[@class='product-name']").ToArray(); 
            // s=document
            return s;
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Login()
        {

            return View();
        }
        public ActionResult Create()
        {

            return View();
        }
        public void SaveImg(string Url)
        {

        }
        [HttpPost]
        public ActionResult Create(Chaters chater, HttpPostedFileBase ProfileImage)
        {
            string pic = Server.MapPath("~/Content/ProfilePic/" + chater.EmailId + ".png");
            //using (WebClient client = new WebClient())
            //{
            //    if (System.IO.File.Exists(pic))
            //    {
            //        System.IO.File.Delete(pic);
            //        System.IO.File.WriteAllBytes(pic, client.DownloadData("https://static.pexels.com/photos/3247/nature-forest-industry-rails.jpg"));
            //    }
            //    else
            //    {
            //        System.IO.File.WriteAllBytes(pic, client.DownloadData("https://static.pexels.com/photos/3247/nature-forest-industry-rails.jpg"));
            //    }
            //}
            if (c.Chaters.Any(x => x.EmailId == chater.EmailId))
            {
                ModelState.AddModelError("err", "user allready exists...");
                return View();
            }
            c.Chaters.AddOrUpdate(chater);
            c.SaveChanges();
            if (ProfileImage != null)
            {
                if (System.IO.File.Exists(pic))
                {
                    System.IO.File.Delete(pic);
                    ProfileImage.SaveAs(Server.MapPath("~/Content/ProfilePic/" + chater.EmailId + ".png"));
                }
                else
                {
                    ProfileImage.SaveAs(Server.MapPath("~/Content/ProfilePic/" + chater.EmailId + ".png"));
                }
            }
            return View();
        }
        public ActionResult LogOut()
        {
            Session.RemoveAll();
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult Login(Chaters chater)
        {
            if (c.Chaters.Any(x => x.EmailId == chater.EmailId && x.Password == chater.Password))
            {
                Session["EmailId"] = chater.EmailId;
                Session["Password"] = chater.Password;
                return RedirectToAction("Chat");
            }
            //c.Chaters.AddOrUpdate(new Chaters() { Id = name, Name = name });
            //c.SaveChanges();
            //Session["EmailId"] = name;
            Response.Write("<script>alert('Login Faild');</script>");
            return View();
        }
        public ActionResult Chat()
        {
            if (Session["EmailId"] == null)
                return RedirectToAction("Login");
            return View();
        }
        public ActionResult getChats(string user, string msgFor)
        {
            if (user == "")
                return RedirectToAction("Login");
            List<object> obj = new List<object>();
            obj.Add(c.Chaters.ToList());
            List<Chat> chats = new List<Chat>();
            if (msgFor == "General")
                chats = (from chat in c.Chat where (chat.massegeFor == msgFor) select chat).OrderBy(x => x.Id).ToList();
            else
                chats = (from chat in c.Chat where ((chat.massegeFor == user && chat.User == msgFor) || (chat.User == user && chat.massegeFor == msgFor)) select chat).OrderBy(x => x.massegeDate).ToList();
            obj.Add(chats);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveChats(string user, string msg, string msgFor)
        {
            if (msgFor == "")
                return Json("select user first", JsonRequestBehavior.AllowGet);
            if (user == "")
                return RedirectToAction("Login");
            c.Chat.Add(new Chat() { User = user, massegeFor = msgFor, Message = msg, massegeDate = DateTime.Now.ToString("hh:mm:ss tt dd/MM/yyyy") });
            c.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}